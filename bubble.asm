#******************************************************
# CSE141 - Su09
# Program to sort array[10]
#
# Author: Dat Pham
#
# void bubbleSort(void)
# {
#    char array[10] = {1, 2,3,4,5,6,7,8,9,10};
#     int array_size = sizeof(array);
#
#   int i, j, temp;
#
#   for (i = (array_size - 1);  i >= 0; i--)
#   {
#     for (j = 1; j <= i; j++)
#     {
#       if (numbers[j-1] > numbers[j])
#       {
#         temp = numbers[j-1];
#         numbers[j-1] = numbers[j];
#         numbers[j] = temp;
#       }
#     }
#   }
# }
#
#
#******************************************************
# use registers $a0~$a7 for address
# use registers $d0~$d7 for data
# use registers $t0~$t7 for scratch pad
#
# $sp: stack pointer
# $ra: return address
# $pc: program counter
#

       .Entry                        # entry point of program

PROG_START:
        MOVI  $a0, 0x1000              # set array base addr to 0x1000
        MOVI  $t0, 0                   # set offset = 0

        MOVI   $d0, 1                  # move a const to $d0
        MEMST  $d0, ($a0), $t0         # store $d0 to $a0[$t0]
        ADDI  $t0, $t0, 1              # increase offset $t0 by 1

        MOVI   $d0, 6                  # move a const to $d0
        MEMST $d0, ($a0), $t0          # store $d0 to $a0[$t0]
        ADDI  $t0, $t0, 1              # increase offset $t0 by 1

        MOVI   $d0, 31                 # move a const to $d0
        MEMST $d0, ($a0), $t0          # store $d0 to $a0[$t0]
        ADDI  $t0, $t0, 1              # increase offset $t0 by 1

        MOVI   $d0, 9                  # move a const to $d0
        MEMST $d0, ($a0), $t0          # store $d0 to $a0[$t0]
        ADDI  $t0, $t0, 1              # increase offset $t0 by 1

        MOVI   $d0, 3                  # move a const to $d0
        MEMST $d0, ($a0), $t0          # store $d0 to $a0[$t0]
        ADDI  $t0, $t0, 1              # increase offset $t0 by 1

        MOVI   $d0, 18                 # move a const to $d0
        MEMST $d0, ($a0), $t0          # store $d0 to $a0[$t0]
        ADDI  $t0, $t0, 1              # increase offset $t0 by 1

        MOVI   $d0, 17                 # move a const to $d0
        MEMST $d0, ($a0), $t0          # store $d0 to $a0[$t0]
        ADDI  $t0, $t0, 1              # increase offset $t0 by 1

        MOVI   $d0, 0                  # move a const to $d0
        MEMST $d0, ($a0), $t0          # store $d0 to $a0[$t0]
        ADDI  $t0, $t0, 1              # increase offset $t0 by 1

        MOVI   $d0, 5                  # move a const to $d0
        MEMST $d0, ($a0), $t0          # store $d0 to $a0[$t0]
        ADDI  $t0, $t0, 1              # increase offset $t0 by 1

        MOVI   $d0, 2                  # move a const to $d0
        MEMST $d0, ($a0), $t0          # store $d0 to $a0[$t0]

BEGIN_SORT:
         MOVI  $a2, 0x1000             # load addr of array to $a2
         MOVI  $t1, 9                  # $t1 = offset 9 of array[10]
         MOVI  $t0, 0                  # $t0 = offset 0 of array[10]

OUTLOOP:
         MOVI  $t2, 1                  # offset 1 of array

INLOOP:
         SUBI  $t3, $t2, 1             # $t3 now points to first element of array

         MEMLD    $d2, ($a2), $t2      # load $a2[$t2] to $d2
         MEMLD    $d3, ($a2), $t3      # load $a2[$t3] to $d3

         BLTE  $d2, $d3, GO_NEXT

         MEMST    $d3, ($a2), $t2      # store $d3 to $a2[$t2]
         MEMST    $d2, ($a2), $t3      # store $d2 to $a2[$t3]

GO_NEXT:
         ADDI  $t2, $t2, 1             # increment inner loop offset $t2
         BLTE  $t2, $t1, INLOOP

         SUBI  $t1, $t1, 1             # decrement outer loop offset $t1
         BGTE  $t1, $t0, OUTLOOP

COUNT_ODD:
        MOVI  $d0, 0                   # use $d0 to count odd numbers
        MOVI  $t2, 10                  # use $t2 as array size
        MOVI  $t0, 0                   # use $t0 as index of array

CHECK_ODD: MEMLD    $d1, ($a2), $t0      # load $a2[$t0] to $d1
         TEST    $d2, $d1, 0           # test if bit0 of $d1 is SET
                                       # if so, $d2=1, else $d2=0
         ADD      $d0, $d0, $d2        # add $d2 to $d0
         ADDI     $t0, $t0, 1          # increment index by 1
         BLT      $t0, $t2, CHECK_ODD

                                       # at the end, $d0 counts
                                       # odd numbers in the array
         B PROG_END

PROG_END:

#       MOV   $pc, $ra                 # return to caller

         .End                          # end of program
